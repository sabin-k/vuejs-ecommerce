const items = [
  {
    id: 1,
    name: "SanDisk 128GB Ultra MicroSDXC UHS-I Memory Card with Adapter",
    price: 19.4,
    category: "Electronics",
    image: "",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    id: 2,
    name: "WD 2TB Elements Portable External Hard Drive HDD, USB 3.0",
    price: 80.43,
    category: "Electronics",
    image:
      "https://images-na.ssl-images-amazon.com/images/I/71VB--jaeSL._AC_SL1500_.jpg",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    id: 3,
    name: "New Apple AirPods Max - Space Gray",
    price: 549.0,
    category: "Electronics",
    image:
      "https://images-na.ssl-images-amazon.com/images/I/81jqUPkIVRL._AC_SL1500_.jpg",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    id: 4,
    name: "Nike Air Max Genome - Men's Shoe",
    price: 170,
    category: "Men's Shoes",
    image:
      "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/51ed7057-0d6e-4fe1-a24a-a12502142830/air-max-genome-shoe-lQ1nNw.png",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },

  {
    id: 5,
    name: "JBL CLIP 3",
    price: 69.95,
    category: "Electronics",
    image:
      "https://www.jbl.com/dw/image/v2/AAUJ_PRD/on/demandware.static/-/Sites-masterCatalog_Harman/default/dw1d003cdd/JBL_Clip3_Front_-Fiesta-Red-1605x1605px.png?sw=537&sfrm=png",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
  {
    id: 6,
    name: "511™ SLIM FIT LEVI’S® FLEX MEN'S JEANS",
    price: 69.5,
    category: "Jeans",
    image:
      "https://lsco.scene7.com/is/image/lsco/levis/clothing/045113623-front-pdp.jpg?fmt=jpeg&qlt=70,1&op_sharpen=0&resMode=sharp2&op_usm=0.8,1,10,0&fit=crop,0&wid=900&hei=1200",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },
];

export default items;
